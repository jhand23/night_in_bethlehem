using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform target;
    public Vector3 offset;
    public float pitch = 2f;

    private float yawSpeed = 100f;
    private float currentZoom = 10f;

    private float currentYaw = 0f;
    
    void Update()
    {
        currentZoom -= Input.GetAxis("Horizantal") * yawSpeed * Time.deltaTime;

    }


    void LateUpdate ()
    {
        transform.position = target.position - offset * currentZoom;
        transform.LookAt(target.position + Vector3.up * pitch);

        transform.RotateAround(target.position, Vector3.up, currentYaw);
    }
    
}
